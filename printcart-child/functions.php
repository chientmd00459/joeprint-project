<?php

function homeone_print_embed_style($css)
{
	$primary_color = printcart_get_options('nbcore_primary_color');
	$text_link = printcart_get_options('nbcore_link_hover_color');
	$text_link_hover = printcart_get_options('nbcore_link_color');
	$css .= "
	.site-footer .footer-bot-section .widget ul li a:before {
		top: 2px;
	}
	.main-navigation .menu-main-menu-wrap #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link {
		font-weight: 600;
		font-family: Poppins;
	}
	.ib-shape .icon{                
		background-color: " . esc_attr($primary_color) . ";
	}
	.vc-blog .blog-content .hb-readmore {
		color: ". esc_attr($text_link) .";
	}
	.vc-blog .blog-content .hb-readmore:hover {
		color: ". esc_attr($text_link_hover) .";
	}
	footer.site-footer .footer-top-section .footer_top_title, .site-footer .footer-bot-section .widget ul li a, footer.site-footer p {
		color: #ccc;
	}
	.woocommerce form .form-row abbr.required {
		text-decoration: none;
		border: none;
	}
	.nbtcs-select {
		top: 27px;
	}
	.nbtcs-w-wrapper .selected {
		padding-left: 3px;
	}
	.vc-blog .blog-content .hb-readmore {
		color: #666;
	}
	.ib-shape .icon i {
		line-height: 65px;
	}
	.comments-area {
		background-color: #fff;
	}
	.nbtcs-w-wrapper .selected {
    		padding-left: 5px;
	}
	.nbtcs-w-wrapper .selected:after {
		margin: 0 0 0 5px;
	}
	@media (min-width: 640px) and (max-width: 768px) {
		.shop_table.cart .actions input.bt-5 {
			float: right;
		}
	}
	@media only screen and (max-width: 768px) {
		body > div#page {
			overflow: hidden;
		}
		.shop_table.cart .cart_item td.product-price, .shop_table.cart .cart_item td.product-quantity {
			padding-right: 30px;
		}
	}
	@media only screen and (max-width: 320px) {
		.middle-section-wrap .middle-right-content .minicart-header .mini-cart-wrap {
			right: -38px !important;
		}
		.header-1 .middle-section-wrap .middle-right-content .header-cart-wrap {
			margin-left: 19px;
		}
		.shop_table.cart td:before {
			position: absolute !important;
			left: 15px !important;
		}
		.shop_table.cart .product-remove {
			text-align: center;
		}
		.shop_table.cart .actions input.bt-5 {
			width: 95%;
		}
	}
	";
	return $css;
}

add_filter('printcart_css_inline', 'homeone_print_embed_style');
add_action( 'after_setup_theme', 'wc_remove_frame_options_header', 11 );
function wc_remove_frame_options_header() {		
	remove_action( 'template_redirect', 'wc_send_frame_options_header' );	
};

function printcart_child_enqueue_styles() {
	wp_enqueue_style('printcart-style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('printcart-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array('printcart-style'),
		wp_get_theme()->get('Version')
	);
	wp_enqueue_style('printcart-style-selena', get_stylesheet_directory_uri() . '/selena.css');
	wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/54ed714a8b.js', array(), 'latest', false);
	wp_enqueue_script('printcart-custom-js', get_stylesheet_directory_uri() . '/js/customize.js', array(), 'latest', false);
}
add_action('wp_enqueue_scripts', 'printcart_child_enqueue_styles');



function coastal_template_single_product_short_description( $post_ID ) {
  $out  = '<div class="product-short-description">';
  $out .= get_the_excerpt();
  // $out .= coastal_short_excerpt(999);
  $out .= '</div>';
  echo $out;
}

// Short Description
add_action( 'coastal_single_product_quick_look', 'coastal_template_single_product_short_description', 10 );

